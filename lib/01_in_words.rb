class Fixnum
  @@nums = {
    0 => "zero",
    1 => "one",
    2 => "two",
    3 => "three",
    4 => "four",
    5 => "five",
    6 => "six",
    7 => "seven",
    8 => "eight",
    9 => "nine",
    10 => "ten",
    11 => "eleven",
    12 => "twelve",
    13 => "thirteen",
    14 => "fourteen",
    15 => "fifteen",
    16 => "sixteen",
    17 => "seventeen",
    18 => "eighteen",
    19 => "nineteen",
    20 => "twenty",
    30 => "thirty",
    40 => "forty",
    50 => "fifty",
    60 => "sixty",
    70 => "seventy",
    80 => "eighty",
    90 => "ninety",
  }

  def in_words
    return "zero" if self.zero?
    if self < 1_000_000_000_000
      self.convert_billions
    elsif self % 1_000_000_000_000 == 0
      @@nums[self / 1_000_000_000_000] + " trillion"
    else
      (self / 1_000_000_000_000).convert_hundreds + " trillion " +
        (self % 1_000_000_000_000).convert_billions
    end
  end

  def convert_billions
    if self < 1_000_000_000
      self.convert_millions
    elsif self % 1_000_000_000 == 0
      @@nums[self / 1_000_000_000] + " billion"
    else
      (self / 1_000_000_000).convert_hundreds + " billion " +
        (self % 1_000_000_000).convert_millions
    end
  end

  def convert_millions
    if self < 1_000_000
      self.convert_thousands
    elsif self % 1_000_000 == 0
      @@nums[self / 1_000_000] + " million"
    else
      (self / 1_000_000).convert_hundreds + " million " +
        (self % 1_000_000).convert_thousands
    end
  end

  def convert_thousands
    if self < 1000
      self.convert_hundreds
    elsif self % 1000 == 0
      @@nums[self / 1000] + " thousand"
    else
      (self / 1000).convert_hundreds + " thousand " +
        (self % 1000).convert_hundreds
    end
  end

  def convert_hundreds
    if self < 100
      self.convert_tens
    elsif self % 100 == 0
      @@nums[self / 100] + " hundred"
    else
      @@nums[self / 100] + " hundred " + (self % 100).convert_tens
    end
  end

  def convert_tens
    if @@nums.key?(self)
      @@nums[self]
    else
      @@nums[self / 10 * 10] + " " + @@nums[self % 10]
    end
  end
end
